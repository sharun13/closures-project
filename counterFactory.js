function counterFactory() {
    let counterVariable = 0;
    returnObject = {
        increment() {
            counterVariable += 1;
            return counterVariable;
        },
        decrement() {
            counterVariable -= 1;
            return counterVariable;
        }
    }
    return returnObject;
}

module.exports = counterFactory();
