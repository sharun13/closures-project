function cacheFunction(callBack) {
    let cache = {};
    function returnCache(argument) {
        for (let key in cache) {
            if (key === argument) {
                return cache;
            }
        }
        cache[argument] = callBack(argument);
        return cache;
    }
    return returnCache;
}

module.exports = cacheFunction;