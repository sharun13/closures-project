let cacheFunction = require('../cacheFunction');

function callBack(argument) {
    if (argument % 2 === 0) {
        return true;
    } else {
        return false;
    }
}

const returnCache = cacheFunction(callBack);

let testArray = [1,2,3,4,5,3,4,6,4,5];
let result;

for (let index = 0; index < testArray.length; index++) {
    result = returnCache(testArray[index]); 
}

console.log(result);