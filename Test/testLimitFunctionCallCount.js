let limitFunctionCallCount = require('../limitFunctionCallCount');

function callBack() {
    return (a + b);
}

const executionLimiter = limitFunctionCallCount(callBack, 5);

let a = 5;
let b = 10;
for (let index  = 0; index < 10; index++) {
    let newVariable = executionLimiter(a, b);
    console.log(newVariable);
    a++;
    b++;
}