let counterFactory = require('../counterFactory');

counterFactory.increment();
counterFactory.increment();

console.log(counterFactory.increment());
console.log(counterFactory.decrement());