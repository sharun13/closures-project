function limitFunctionCallCount(callBack, number) {
    let counter = 0;
    if (number == undefined) {
        number = 1;
    }
    function executionLimiter(a, b) {
        if (counter < number) {
            counter +=1;
            let sum = callBack(a, b);
            return sum;
        } else {
            return null;
        }
    }
    return executionLimiter;
}

module.exports = limitFunctionCallCount;
